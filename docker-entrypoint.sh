#!/bin/sh

echo "Custom entrypoint biatch $@"

case "$1" in

	get-boilerplate)
		echo "Getting boilerplate..."
		cp -R /react-app-boilerplate/public/* /react-app/public/
		cp -R /react-app-boilerplate/src/* /react-app/src/
		cat /react-app-boilerplate/package.json > /react-app/package.json
		;;

	start)
		echo "Starting app..."
		npm start
		;;

esac