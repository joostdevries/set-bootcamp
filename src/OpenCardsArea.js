import React, { Component } from 'react';
import SetCard from './SetCard';

class OpenCardsArea extends Component {
  constructor(props) {
    super(props);
    this.selectCard = this.selectCard.bind(this);
  }

  selectCard(card){
    this.props.onSelectCard(card);
  }

  SVGPattern(){
    return (
      <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%">
        <defs>
          <pattern className="svg-pattern svg-pattern-red" id="svg-pattern-red" patternUnits="userSpaceOnUse" width="6" height="4">
            <line x1="0" y="0" x2="0" y2="8.5" />
          </pattern>
          <pattern className="svg-pattern svg-pattern-purple" id="svg-pattern-purple" patternUnits="userSpaceOnUse" width="6" height="4">
            <line x1="0" y="0" x2="0" y2="8.5" />
          </pattern>
          <pattern className="svg-pattern svg-pattern-green" id="svg-pattern-green" patternUnits="userSpaceOnUse" width="6" height="4">
            <line x1="0" y="0" x2="0" y2="8.5" />
          </pattern>
        </defs>
      </svg>
    )
  }

  render() {
    return (
      <div className={"open-cards" + (this.props.cards.length>12 ? " extra-cards" : "")}>
        {this.props.cards.map((card, key) =>
          <SetCard isSelected={this.props.selectedCards.indexOf(card)!==-1} onSelect={this.selectCard} card={card} key={key} />
        )}
        <this.SVGPattern />
      </div>)
  }
}

export default OpenCardsArea;
