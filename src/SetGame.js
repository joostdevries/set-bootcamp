import React, { Component } from 'react';
import OpenCardsArea from './OpenCardsArea';
import SetCard from './SetCard';

const SET_COLORS = ['red', 'purple', 'green'];
const SET_SHAPES = ['diamond', 'stadium', 'wave'];
const SET_FILLS = ['transparent', 'striped', 'solid'];
const SET_SHAPE_COUNTS = [1, 2, 3];

const i3 = (i1, i2) => (i1===i2 ? i1 : (Math.abs(i1-i2)===2 ? 1 : (i1+i2===3 ? 0 : 2)))

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

class SetGame extends Component {
  constructor(props) {
    super(props);
    let deck = shuffle(this.createDeck());
    let openCards = deck.splice(0, 12);
    this.state = {
      deck,
      openCards,
      selectedCards: [],
      playerSets: [],
      computerSets: [],
      playerPenaltySets: [],
      playerSelecting: false,
      computerSelecting: false
    };
    this.addOpenCards = this.addOpenCards.bind(this);
    this.drawCardsFromDeck = this.drawCardsFromDeck.bind(this);
    this.playerSelectCard = this.playerSelectCard.bind(this);
  }

  addOpenCards(cards){
    this.setState({openCards: [...this.state.openCards, cards]})
  }
  
  drawCardsFromDeck(nCards) {
    let deck = this.state.deck;
    let cards = deck.splice(0, nCards);
    this.setState({deck})
    return cards;
  }

  createDeck() {
    let deck = [];
  	SET_COLORS.forEach((color, iCol) => {
  		SET_SHAPES.forEach((shape, iShp) => {
  			SET_FILLS.forEach((fill, iFil) => {
  				SET_SHAPE_COUNTS.forEach((shapeCount, iShc) => {
            deck.push({
              color: iCol,
              shape: iShp,
              fill: iFil,
              shapeCount: iShc,
              bits: (iCol<<6) + (iShp<<4) + (iFil<<2) + (iShc)
            });
  				});
  			});
  		});
  	});
 
  	return deck;
  }

  replaceCards(cards, oldCards, newCards) {
    oldCards.forEach((card, i) => {
      if(newCards[i]) {
        cards[cards.indexOf(card)] = newCards[i]
      }
      else {
        cards.splice(cards.indexOf(card), 1);
      }
    });
    return cards;
  }

  getPropertiesWithoutSet(cards) {
    let propertiesWithoutSet = [];
    ['color', 'shape', 'fill', 'shapeCount'].forEach(prop => {
      if(new Set(cards.map(card => card[prop])).size === 2) {
        propertiesWithoutSet.push(prop);
      }
    });
    return propertiesWithoutSet;
  }

  validateSelectedCards() {
    let selectedCards = this.state.selectedCards;
    let propertiesWithoutSet = this.getPropertiesWithoutSet(selectedCards);

    if(propertiesWithoutSet.length === 0) {
      if(this.state.computerSelecting) {
        this.setState({
          selectedCards: [],
          computerSets: [...this.state.computerSets, selectedCards],
          openCards: this.replaceCards(this.state.openCards, selectedCards, this.state.openCards.length<=12 ? this.drawCardsFromDeck(3) : [])
        });
      }
      else {
        this.setState({
          selectedCards: [],
          playerSets: [...this.state.playerSets, selectedCards],
          openCards: this.replaceCards(this.state.openCards, selectedCards, this.state.openCards.length<=12 ? this.drawCardsFromDeck(3) : [])
        });
      }
    }
    else {
      alert('No set! 💩 You have 2 cards with the same ' + propertiesWithoutSet.join(' and '));
      this.setState({
        selectedCards:[],
        playerPenaltySets: [...this.state.playerPenaltySets, selectedCards]
      });
    }

    this.startGame();
  }

  selectCard(card) {
    let selectedCards = this.state.selectedCards;
    if(selectedCards.indexOf(card)!==-1) {
      return; 
    }

    selectedCards.push(card);
    this.setState({selectedCards});
    if(selectedCards.length === 3) {
      setTimeout(() => {
        this.validateSelectedCards();
      },this.state.playerSelecting?500:3000);
    }
  }

  computerSelectSet() {
    this.clearComputerTimer();
    this.setState({playerSelecting:false, computerSelecting:true});
    let firstFoundSetCards = this.findFirstSet(this.state.openCards);
    if (firstFoundSetCards) {
      let computerSelectInterval;
      computerSelectInterval = setInterval(() => {
        let card = firstFoundSetCards.splice(0,1)[0];
        if(firstFoundSetCards.length===0) {
          clearInterval(computerSelectInterval);
        }
        this.selectCard(card);
      },1000);
    }
    else {
      this.setState({
        openCards: [...this.state.openCards, ...this.drawCardsFromDeck(3)]
      });
      this.startGame();
    }
  }

  findFirstSet(cards) {
    let cardsByBits = {};
    cards.forEach(card => {cardsByBits[card.bits]=card});
    let head = cards.slice(0, cards.length - 1);
    for (let i=0; i< head.length; i++) {
      let firstCard = head[i];
      let tail = cards.slice(i + 1, cards.length);
      for(var j=0; j<tail.length; j++) {
        let secondCard = tail[j];
        let i3Col = i3(firstCard.color, secondCard.color);
        let i3Shp = i3(firstCard.shape, secondCard.shape);
        let i3Fil = i3(firstCard.fill, secondCard.fill);
        let i3Shc = i3(firstCard.shapeCount, secondCard.shapeCount);
        let thirdCardBits = (i3Col<<6) + (i3Shp<<4) + (i3Fil<<2) + (i3Shc);
        if(cardsByBits[thirdCardBits]) {
          return [firstCard, secondCard, cardsByBits[thirdCardBits]];
        }
      }
    }
  }

  startGame() {
    this.setState({playerSelecting:true, computerSelecting:false});
    this.startComputerTimer();
  }

  startComputerTimer() {
    if(this.timerId) {
      clearTimeout(this.timerId);
    }
    this.timerId = setTimeout(() => this.computerSelectSet(), 10000);
  }

  clearComputerTimer() {
    clearTimeout(this.timerId);
  }

  playerSelectCard(card) {
    if(this.state.playerSelecting) {
      this.selectCard(card);
      this.clearComputerTimer();
    }
  }

  componentDidMount(){
    window.addEventListener('keyup', e => {if(e.keyCode===32) {this.playerCallsSet()}})
    this.startGame();
  }

  componentWillUnmount() {
    this.clearComputerTimer()
    window.removeEventListener('keyup')
  }

  render() {
    return <div className={"set-game" + (this.state.playerSelecting ? " players-turn" : "") + (this.state.computerSelecting ? " computers-turn" : "")}>
      <OpenCardsArea selectedCards={this.state.selectedCards} onSelectCard={this.playerSelectCard} cards={this.state.openCards} />
      <div className="score-panel">
        <div className="score player-score">
          <h2>You: {this.state.playerSets.length - this.state.playerPenaltySets.length}</h2>
          <ul className="player-sets">
          {this.state.playerSets.slice().reverse().map((playerSet, key) =>
          <li key={key} className="picked-set">
            {playerSet.map((card, key) =>
              <SetCard card={card} key={key} />
            )}
          </li>
          )}
          </ul>
        </div>
        <div className="score computer-score">
          <h2>Computer: {this.state.computerSets.length}</h2>
          <ul className="computer-sets">
          {this.state.computerSets.slice().reverse().map((playerSet, key) =>
          <li key={key} className="picked-set">
            {playerSet.map((card, key) =>
              <SetCard card={card} key={key} />
            )}
          </li>
          )}
          </ul>
        </div>
      </div>
    </div>
  }
}

export default SetGame;
export {SET_COLORS, SET_SHAPES, SET_FILLS, SET_SHAPE_COUNTS};