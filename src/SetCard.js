import React, { Component } from 'react';
import {SET_COLORS, SET_SHAPES, SET_FILLS, SET_SHAPE_COUNTS} from './SetGame';

class SetCard extends Component {
  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
  }

  getCardClassNames() {
    return `card card-${SET_SHAPE_COUNTS[this.props.card.shapeCount]}-shapes`
      + ` card-${SET_COLORS[this.props.card.color]}`
      + ` card-${SET_FILLS[this.props.card.fill]}`
      + ` card-${SET_SHAPES[this.props.card.shape]}`
      + (this.props.isSelected ? ' card-selected' : '');
  }

  getShape(shapeName) {
    const stadium = (<svg viewBox="-5 -5 110 60">
        <g className="svg-shape">
          <rect id="stadium-shape" x="0" y="0" width="100" height="40" rx="20"></rect>
        </g>
      </svg>)


    const wave = (<svg viewBox="-5 -5 110 60">
        <g className="svg-shape" >
          <path d="M72.1033868,40 C79.8871064,40 86.6013072,37.0021413 92.2459893,31.006424 C97.4153298,25.6388294 100,19.9286224 100,13.875803 C100,6.62384011 96.1081402,2.99785867 88.3244207,2.99785867 C82.9174094,2.99785867 78.7878788,6.70949322 75.9358289,14.1327623 C74.2127154,18.6438258 73.1134878,20.8708066 72.6381462,20.8137045 C69.1325015,20.5852962 65.5377302,18.6723769 61.8538324,15.0749465 C55.4961378,8.7366167 52.1687463,5.45324768 51.8716578,5.2248394 C47.2964944,1.74161313 42.097445,0 36.2745098,0 C28.6096257,0 21.3903743,2.51249108 14.6167558,7.53747323 C9.09090909,11.6488223 4.63458111,16.7594575 1.24777184,22.869379 C0.415923945,24.3540328 0,25.7815846 0,27.1520343 C0,29.7787295 1.24777184,32.0057102 3.74331551,33.8329764 C6.06060606,35.5460385 8.76411171,36.4025696 11.8538324,36.4025696 C16.0724896,36.4025696 19.3998812,34.6895075 21.8360071,31.2633833 C27.6589424,23.0406852 32.4717766,18.9293362 36.2745098,18.9293362 L36.2745098,18.9293362 L38.2352941,21.0706638 C44.4147356,27.6945039 49.0493167,32.0342612 52.1390374,34.0899358 C57.9619727,38.0299786 64.6167558,40 72.1033868,40 Z" id="path-1"></path>
        </g>
      </svg>)

    const diamond = (<svg viewBox="-5 -5 110 60">
        <g className="svg-shape" >
          <polygon points="0 20 50 0 100 20 50 40"></polygon>
        </g>
    </svg>)

    const shapes = {
      stadium,
      wave,
      diamond
    }
  
    return shapes[shapeName]
  }
  
  onSelect(e) {
    if(this.props.onSelect) {
      this.props.onSelect(this.props.card);
    }
  }

  render() {
    let shapes = [];
    for(let i=0; i<SET_SHAPE_COUNTS[this.props.card.shapeCount]; i++) {
      shapes.push(
        <div key={i} className='shape'>
          {this.getShape(SET_SHAPES[this.props.card.shape])}
        </div>
      )
    }
    return (
      <div onClick={this.onSelect} className={this.getCardClassNames(this.props.card)}>
        {shapes}
      </div>
    )
  }

}

export default SetCard;