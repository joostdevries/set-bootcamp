FROM node:8.2.1-alpine
RUN npm install -g create-react-app \
                   create-react-native-app \
                   react-native-cli
WORKDIR /
RUN create-react-app react-app
RUN mkdir /react-app-boilerplate
RUN cp -R /react-app/src /react-app-boilerplate/src
RUN cp -R /react-app/public /react-app-boilerplate/public
RUN cp /react-app/package.json /react-app-boilerplate/package.json
COPY docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh
WORKDIR /react-app
